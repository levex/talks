#!/bin/bash

WHAT=$1
WHERE=$2

[[ -z $WHAT ]] && echo Usage: $0 \<what\> \<to-where\> && exit 1
[[ -z $WHERE ]] && echo Usage: $0 \<what\> \<to-where\> && exit 1

_TMPFILE=$(mktemp)

qpdf --empty --pages ${WHAT} 1 -- ${_TMPFILE}
gs -o ${WHERE} -sDEVICE=jpeg -dLastPage=1 ${_TMPFILE}

rm ${_TMPFILE}
