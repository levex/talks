---
layout: post
title: Introduction to Linux Kernel development - Your First Patch!
date: 2014-02-09 13:32:20 +0300
description: >
      <strong>DevConf 2014</strong>, Brno, Czech Republic
img: devconf14_post.jpg # Add image post (optional)
fig-caption: Booyah. # Add figcaption (optional)
tags: [test]
---

I joined my friends at the Fedora Project for a weekend of DevConf 2014 in Brno.
Soon after, I would accept an intern offer at the Red Hat offices in the same city.
Many thanks to DevConf for sponsoring my trip!

<center>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/158ocg6hQbmolP" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>

</center>
Sadly, not the entirety of the talk was recorded, however what was recorded is
available here: <a href="https://www.youtube.com/watch?v=R6wjfOgE6Rs">https://www.youtube.com/watch?v=R6wjfOgE6Rs</a>

The slides in PDF format can be downloaded here:
<a href="https://talks.kernelstuff.org/assets/slides/lkurusa_intro_to_linux_kernel_development_devconf14.pdf">https://talks.kernelstuff.org/assets/slides/lkurusa_intro_to_linux_kernel_development_devconf14.pdf</a>
