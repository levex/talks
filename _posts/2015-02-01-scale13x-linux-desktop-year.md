---
layout: post
title: When is the Year of the Linux Desktop?
date: 2015-02-01 13:32:20 +0300
description: >
      <strong>SCALE 13x</strong>, Los Angeles, CA
img: scale13x_post.jpg # Add image post (optional)
fig-caption: Booyah. # Add figcaption (optional)
tags: [test]
---

I gave this talk at SCALE 13x, which was my first visit both to the conference,
and to California. Huge thanks to SCALE for sponsoring my trip there!

<center>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/o1fAUaHyqrUsfR" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>

</center>
The talk was recorded and available at:
<a href="https://www.youtube.com/watch?v=dFybF8QaT-M">https://www.youtube.com/watch?v=dFybF8QaT-M</a>

The slides in PDF format can be downloaded here:
<a href="https://talks.kernelstuff.org/assets/slides/linux_desktop_when_is_our_year.pdf lkurusa_when_is_the_year_of_the_linux_desktop_scale13x.pdf">https://talks.kernelstuff.org/assets/slides/lkurusa_lets_write_a_debugger_lca2018.pdf</a>
