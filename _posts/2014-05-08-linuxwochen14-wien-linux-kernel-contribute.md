---
layout: post
title: Linux Kernel - Let's Contribute!
date: 2014-05-08 13:32:20 +0300
description: >
      <strong>Linuxwochen 2014</strong>, Wien, Austria
img: lw14_wien_post.jpg # Add image post (optional)
fig-caption: Booyah. # Add figcaption (optional)
tags: [test]
---

At some point in the past I gave a bunch of talks trying to get people to
start contributing to the kernel. It's really not that hard to contribute.
This talk was well received and I enjoyed the audience a lot. Many thanks to
the Fedora Project for sponsoring my travel and stay!

<center>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/3R1XpYoSb0yd5h" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>

</center>
Sadly, as far as I'm aware the talk was not recorded.

The slides in PDF format can be downloaded here:
<a href="https://talks.kernelstuff.org/assets/slides/lkurusa_linux_kernel_lets_contribute_linuxwochen2014.pdf">https://talks.kernelstuff.org/assets/slides/lkurusa_linux_kernel_lets_contribute_linuxwochen2014.pdf</a>
