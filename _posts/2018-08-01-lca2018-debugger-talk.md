---
layout: post
title: Let's write a Debugger!
date: 2018-01-26 13:32:20 +0300
description: >
      <strong>linux.conf.au 2018</strong>, Sydney, Australia
img: lca2018_post.png # Add image post (optional)
fig-caption: Booyah. # Add figcaption (optional)
tags: [test]
---

I gave this talk at linux.conf.au 2018, in Sydney, Australia. This was my first
visit to this conference and to the continent of Australia. I am eternally
grateful for the organizing committee of LCA, for their support in making sure
I can make the trek to Australia.

<center>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/1a8xxtRyNhMaK4" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>
</center>
The talk was recorded and available at:
<a href="https://www.youtube.com/watch?v=qS51kIHWARM">https://www.youtube.com/watch?v=qS51kIHWARM</a>

The slides in PDF format can be downloaded here:
<a href="https://talks.kernelstuff.org/assets/slides/lkurusa_lets_write_a_debugger_lca2018.pdf">https://talks.kernelstuff.org/assets/slides/lkurusa_lets_write_a_debugger_lca2018.pdf</a>

The folks over at opensource.com invited me to do a blog post about this talk:
<a href="https://opensource.com/article/18/1/how-debuggers-really-work">https://opensource.com/article/18/1/how-debuggers-really-work</a>

Finally, the resources used in the talk are available in the GitHub repository at:
<a href="https://github.com/levex/debugger-talk">https://github.com/levex/debugger-talk</a>
