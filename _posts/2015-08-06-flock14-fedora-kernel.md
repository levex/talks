---
layout: post
title: How is the Fedora kernel different?
date: 2015-08-06 13:32:20 +0300
description: >
      <strong>Flock 2014</strong>, Prague, Czech Republic
img: flock2014_post.jpg # Add image post (optional)
fig-caption: Booyah. # Add figcaption (optional)
tags: [test]
---

Before the conference, I was wondering what were the differences between the
many distributions' kernels. This talk is a manifestation of that. Huge thanks
to Red Hat and the Fedora Project for sponsoring my accommodation and travel!

<center>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/oA0cHtAokeqq3b" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>

</center>
The talk was recorded and available at:
<a href="https://www.youtube.com/watch?v=O4vj_hyLok0">https://www.youtube.com/watch?v=O4vj_hyLok0</a>

The slides in PDF format can be downloaded here:
<a href="https://talks.kernelstuff.org/assets/slides/flock2014_how_is_the_fedora_kernel_different.pdf">https://talks.kernelstuff.org/assets/slides/flock2014_how_is_the_fedora_kernel_different.pdf</a>
